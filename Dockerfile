# https://docs.docker.com/engine/reference/builder/
# FROM refer the image where we start from
# Here we will use a basic python 3 debian image

FROM python:3

# Use the RUN command to pass command to be run
# Here we update the image and add python3 virtualenv

RUN apt-get update && apt-get upgrade -y && apt-get install \
  -y --no-install-recommends python3

# Install pip dependencies
ADD requirements.txt /app/requirements.txt
RUN pip install --default-timeout=100 --upgrade pip && pip install -r /app/requirements.txt

# We add the current content of the git repo in the /app directory
ADD . /app
WORKDIR /app
RUN rm -rf .gitlab-ci.yml Dockerfile README.md img solution
# We use the CMD command to start the gunicorn daemon
# when we start the container.
# Note the $PORT variable, we will need to define it when we start the container
CMD python manage.py runserver 0.0.0.0:$PORT